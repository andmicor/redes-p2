#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <arpa/inet.h>


#define MSG_SIZE 250
#define MAX_CLIENTS 50


/*
 * El servidor ofrece el servicio de un chat
 */

struct Jugador{

	char nombre[50];
	int socket;
	int estado;//PROPONER 5 ESTADOS: CONECTADO, USUARIO, VALIDADO, PARTIDA INDV-GRUPAL
	int puntuacion;
	// 0 -> Conectado
	// 1 -> Usuario
	// 2 -> Validado
	// 3 -> Part. Indiv
	// 4 -> Part. Grupal

};

struct Partida
{
	char frase_original[1500];//Frase original del fichero
	char panel[1500];//Panel que se codifica con guiones
	char resuelto[1500];//PAnel que se resuelve poco a poco
	int encontrado;//Si existe una vocal o consonante
};

void manejador(int signum);
void salirCliente(int socket, fd_set * readfds, int * numClientes, int arrayClientes[]);
int comprobarJugadorUser(char cadNombre[50]);
int validarJugadorPassword(char cadPass[50], char n[50]);
int registrarJugador(char *user,char *pass);
struct Partida devolverFrase();
struct Partida destapar(struct Partida p, char letra);
int buscaSocket(int i, int numClientes, struct Jugador *jugadores);

int comprobacionLineaOrdenes(char buffer[MSG_SIZE]);





int main ( )
{
	srand(time(NULL));
  system("clear");
	/*---------------------------------------------------- 
		Descriptor del socket y buffer de datos                
	-----------------------------------------------------*/
	int sd, new_sd;
	struct sockaddr_in sockname, from;
	char buffer[MSG_SIZE];
	socklen_t from_len;
	fd_set readfds, auxfds;
	int salida;
	int arrayClientes[MAX_CLIENTS];
	int numClientes = 0;
	//contadores
	int i,j,k;
	int recibidos;
	char identificador[MSG_SIZE];


	 // VARIABLES USADAS EN EL JUEGO DE LA RULETA
	char vocal;
	struct Partida p;
	char consonante;
	char resolver[1500]; //Cadena que escribe el usuario que quiere resolver el panel
	p.encontrado=0; //Para saber si esta la consonante o vocal
	char estadisticas[1500];

	int intento=0;//Inicializamos el numero de intentos

	int on, ret;

	/*---------------------------------------------------- 
		Variables separadoras USUARIO Y PASSWORD
	---------------------------------------------*/
	char s1[50];
	char s2[50];
	char s3[50];
	char s4[50];
	char s5[50];

	/* Variable que encuentra jugador y registro OK*/

	int enc;

	int reg;	
	
	/* Vector de estructuras tipo jugadores que contiene a los clientes que inician juego*/
	struct Jugador jugadores[MAX_CLIENTS];

	/*Posición en la que se encuentra el cliente en el vector de estructuras*/
	int pos;
    
	/* --------------------------------------------------
		Se abre el socket 
	---------------------------------------------------*/
  	sd = socket (AF_INET, SOCK_STREAM, 0);
	if (sd == -1)
	{
		perror("No se puede abrir el socket cliente\n");
    		exit (1);	
	}
    
    // Activaremos una propiedad del socket que permitir· que otros
    // sockets puedan reutilizar cualquier puerto al que nos enlacemos.
    // Esto permitir· en protocolos como el TCP, poder ejecutar un
    // mismo programa varias veces seguidas y enlazarlo siempre al
    // mismo puerto. De lo contrario habrÌa que esperar a que el puerto
    // quedase disponible (TIME_WAIT en el caso de TCP)
    on=1;
    ret = setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));



	sockname.sin_family = AF_INET;
	sockname.sin_port = htons(2050);
	sockname.sin_addr.s_addr =  INADDR_ANY;

	if (bind (sd, (struct sockaddr *) &sockname, sizeof (sockname)) == -1) // Asigna puerto
	{
		perror("Error en la operación bind");
		exit(1);
	}
	

   	/*---------------------------------------------------------------------
		Del las peticiones que vamos a aceptar sólo necesitamos el 
		tamaño de su estructura, el resto de información (familia, puerto, 
		ip), nos la proporcionará el método que recibe las peticiones.
   	----------------------------------------------------------------------*/
		from_len = sizeof (from);


		if(listen(sd,1) == -1){
			perror("Error en la operación de listen");
			exit(1);
		}
    
    //Inicializar los conjuntos fd_set
    FD_ZERO(&readfds);
    FD_ZERO(&auxfds);
    FD_SET(sd,&readfds);
    FD_SET(0,&readfds);
    
   	
    //Capturamos la señal SIGINT (Ctrl+c)
    signal(SIGINT,manejador);
    
	/*-----------------------------------------------------------------------
		El servidor acepta una petición
	------------------------------------------------------------------------ */
		while(1){
            
		    //Esperamos recibir mensajes de los clientes (nuevas conexiones o mensajes de los clientes ya conectados)
		    
		    auxfds = readfds;
		    
		    salida = select(FD_SETSIZE,&auxfds,NULL,NULL,NULL);
		    
		    if(salida > 0){
		        
		        
		        for(i=0; i<FD_SETSIZE; i++){
		            
		            //Buscamos el socket por el que se ha establecido la comunicación
		            if(FD_ISSET(i, &auxfds)) {
		                
		                if( i == sd){
		                    
		                    if((new_sd = accept(sd, (struct sockaddr *)&from, &from_len)) == -1){
		                        perror("Error aceptando peticiones");
		                    }
		                    else
		                    {
		                        if(numClientes < MAX_CLIENTS){
		                            arrayClientes[numClientes] = new_sd;

					    jugadores[numClientes].socket=new_sd;
					    jugadores[numClientes].estado=0;//eSTADO SIN REGISTRARSE
			
		                            numClientes++;
		                            FD_SET(new_sd,&readfds);
		                        
		                            strcpy(buffer, "+0k. Usuario conectado\n");
		                        
		                            send(new_sd,buffer,strlen(buffer),0);
		                        
		                            for(j=0; j<(numClientes-1);j++){
		                            
		                                bzero(buffer,sizeof(buffer));
		                                sprintf(buffer, "Nuevo Cliente conectado: %d\n",new_sd);
		                                send(arrayClientes[j],buffer,strlen(buffer),0);
		                            }
		                        }
		                        else
		                        {
		                            bzero(buffer,sizeof(buffer));
		                            strcpy(buffer,"Demasiados clientes conectados\n");
		                            send(new_sd,buffer,strlen(buffer),0);
		                            close(new_sd);
		                        }
		                        
		                    }
		                    
		                    
		                }
		                else if (i == 0){
		                    //Se ha introducido información de teclado
		                    bzero(buffer, sizeof(buffer));
		                    fgets(buffer, sizeof(buffer),stdin);
		                    
		                    //Controlar si se ha introducido "SALIR", cerrando todos los sockets y finalmente saliendo del servidor. (implementar)
		                    if(strcmp(buffer,"SALIR\n") == 0){
		                     
		                        for (j = 0; j < numClientes; j++){
		                            send(arrayClientes[j], "Desconexion servidor\n", strlen("Desconexion servidor\n"),0);
		                            close(arrayClientes[j]);
		                            FD_CLR(arrayClientes[j],&readfds);
		                        }
		                            close(sd);
		                            exit(-1);
		                        
		                        
		                    }
		                    //Mensajes que se quieran mandar a los clientes (implementar)
		                    
		                } 
		                else{ // AQUÍ VA TODO EL PROGRAMA

		                    bzero(buffer,sizeof(buffer));
		                    
		                    recibidos = recv(i,buffer,sizeof(buffer),0);
		                    int pos=buscaSocket(i,numClientes,jugadores);

		                    
		                    if(recibidos > 0){
		                    	
					if(strncmp(buffer,"REGISTRO", 8)==0){//NO HACE FALTA ESTAR VALIDADO PARA PODER REGISTRARSE
		                          
							sscanf(buffer,"REGISTRO -u %s -p %s",s1,s2);


							reg=registrarJugador(s1,s2);
							

							

								if(reg==1){
								send(i, "+Ok.Usuario registrado\n", strlen("+Ok.Usuario registrado\n"),0);
										}
							else{
								send(i, "-Err: Usuario NO registrado\n", strlen("-Err: Usuario NO registrado\n"),0);
					}

							    
		                            
		                        }

				else if(strncmp(buffer,"USUARIO", 7) == 0 && jugadores[pos].estado==0){
		                            

							printf("\n");							

							sscanf(buffer,"%s %s", s1, s2 );

								enc=comprobarJugadorUser(s2);

								if(enc==1){
									 send(i, "+Ok. Usuario correcto\n", strlen("+Ok. Usuario correcto\n"),0);
									 jugadores[pos].estado=1;//eL ESTADO PASA A CONECTADO
									 strcpy(jugadores[pos].nombre,s2);
								}
								else{	
									send(i, "–Err. Usuario incorrecto\n", strlen("–Err. Usuario incorrecto\n"),0);				
								}					
							 
		                        }
					else if(strncmp(buffer,"PASSWORD", 8) == 0  && jugadores[pos].estado==1){
		                            


							printf("\n");							

							sscanf(buffer,"%s %s", s1, s2 );

							enc=validarJugadorPassword(s2,jugadores[pos].nombre);
							

								if(enc==1){
									send(i, "+Ok. Usuario validado\n", strlen("+Ok. Usuario validado\n"),0);
									jugadores[pos].estado=2;//YA SE HA VALIDADO EN EL REGISTRO
									//printf(" estado partida :%d\n", jugadores[i].estado);
								}
								
								else
									send(i, "–Err. Error en la validación\n", strlen("–Err. Error en la validación\n"),0);		
								
								
				           
		                            
		                        }

				   else if(strncmp(buffer,"PARTIDA-INDIVIDUAL",18)==0 && jugadores[pos].estado==2)
				   {
				   			
						             

						             	printf("PARTIDA-INDIVIDUAL, jugando : %s\n", jugadores[pos].nombre);

						             	jugadores[pos].estado=3;	//PASAMOS AL ESTADO PARTIDA INDIVIDUAL

						             

						     
						             	p=devolverFrase();
						             	strcpy(p.resuelto,p.panel);//Copiamos el panel vacio al panel que se resuelve poco a poco solo una vez en el programa
							   			printf("%s\n", p.frase_original);
							   			send(i,"La frase es:\n", strlen("La frase es:\n"),0);
							   			send(i, p.panel, strlen(p.panel),0);

							   		
							   			send(i,"Para jugar introduzca CONSONANTE, VOCAL o RESOLVER:\n", strlen("Para jugar introduzca CONSONANTE, VOCAL o RESOLVER:\n"),0);

						             	
						             
					}


				   else if(strncmp(buffer,"PARTIDA-GRUPO",13)==0 && jugadores[pos].estado==2)
				   {
				   			
						             

						             	printf("PARTIDA-GRUPO, jugando : %s\n", jugadores[pos].nombre);

						             	jugadores[pos].estado=4;	//PASAMOS AL ESTADO PARTIDA INDIVIDUAL

						             

						     
						             	p=devolverFrase();
						             	strcpy(p.resuelto,p.panel);//Copiamos el panel vacio al panel que se resuelve poco a poco solo una vez en el programa
							   			printf("%s\n", p.frase_original);
							   			send(i,"La frase es:\n", strlen("La frase es:\n"),0);
							   			send(i, p.panel, strlen(p.panel),0);

							   		
							   			send(i,"Para jugar introduzca CONSONANTE, VOCAL o RESOLVER:\n", strlen("Para jugar introduzca CONSONANTE, VOCAL o RESOLVER:\n"),0);

						             	
						             
					}




						   else if(strncmp(buffer,"CONSONANTE",10)==0 && (jugadores[pos].estado==3 || jugadores[pos].estado==4))//Siempre que el estado sea partida individual
						        	{
						        		intento++;//INCREMENTAN EL NUMERO DE INTENTOS QUE REALIZA UN USUARIO
						             		sscanf(buffer,"CONSONANTE %c", &consonante);
						             		p=destapar(p,consonante);
						           			printf("Panel descubierto: %s\n", p.resuelto);
						           			send(i,p.resuelto,strlen(p.resuelto),0);

						           			if(p.encontrado==1)
						           				send(i,"+Ok. Existe la consonante\n", strlen("+Ok. Existe la consonante\n"),0);
						           			else
						           				send(i,"+Ok. No existe la consonante\n", strlen("+Ok.No existe la consonante\n"),0);

						           			

						         	}

						    else if(strncmp(buffer,"VOCAL",5)==0 && (jugadores[pos].estado==3 || jugadores[pos].estado==4))
						          {
						          	intento++;//INCREMENTAN EL NUMERO DE INTENTOS QUE REALIZA UN USUARIO
						             		sscanf(buffer,"VOCAL %c", &vocal);
						             		p=destapar(p,vocal);
						             		printf("Panel descubierto: %s\n", p.resuelto);
						             		send(i,p.resuelto,strlen(p.resuelto),0);

						             		if(p.encontrado==1)//Si la vocal se encuentra en la frase
						           				send(i,"+Ok. Existe la vocal\n", strlen("+Ok. Existe la vocal\n"),0);
						           			else
						           				send(i,"+Ok. No existe la vocal\n", strlen("+Ok.No existe la vocal\n"),0);
						        }

						        else if(strncmp(buffer,"RESOLVER",8)==0 && (jugadores[pos].estado==3 || jugadores[pos].estado==4))
						             	{
						             		intento++;//INCREMENTAN EL NUMERO DE INTENTOS QUE REALIZA UN USUARIO
						             		sscanf(buffer,"RESOLVER %[^\t]", resolver);//Cogemos la frase completa

						             		if(strcmp(resolver,p.frase_original)==0)
						             		{
						             			send(i,"+Ok. Le damos la enhorabuena\n", strlen("+Ok. Le damos la enhorabuena\n"),0);
						             			send(i,p.frase_original, strlen(p.frase_original),0);

						             			if(intento<4)
						             			{
						             				jugadores[pos].puntuacion=250;
						             			
						             				sprintf(estadisticas,"Usuario:%s. La puntuacion es la siguiente:%d, numero de intentos:%d\n", jugadores[pos].nombre, jugadores[pos].puntuacion, intento);//Escribimos en el buffer ya que hay que enviar una cadena
						             				send(i,estadisticas,strlen(estadisticas),0);
						             			}

						             			else if(intento>4 && intento <8)
						             			{
						             				jugadores[pos].puntuacion=100;
						             				sprintf(estadisticas,"Usuario:%s. La puntuacion es la siguiente:%d, numero de intentos:%d\n", jugadores[pos].nombre, jugadores[pos].puntuacion, intento);//Escribimos en el buffer ya que hay que enviar una cadena
						             				send(i,estadisticas,strlen(estadisticas),0);
						             			
						             				
						             			}
						             			else if(intento>8 && intento <10)
						             			{
						             				jugadores[pos].puntuacion=70;
						             				sprintf(estadisticas,"Usuario:%s. La puntuacion es la siguiente:%d, numero de intentos:%d\n", jugadores[pos].nombre, jugadores[pos].puntuacion, intento);//Escribimos en el buffer ya que hay que enviar una cadena
						             				send(i,estadisticas,strlen(estadisticas),0);
						             			}
						             			else if(intento>10 && intento <14)
						             			{
						             				jugadores[pos].puntuacion=50;
						             				sprintf(estadisticas,"Usuario:%s. La puntuacion es la siguiente:%d, numero de intentos:%d\n", jugadores[pos].nombre, jugadores[pos].puntuacion, intento);//Escribimos en el buffer ya que hay que enviar una cadena
						             				send(i,estadisticas,strlen(estadisticas),0);
						             			}
						             			else if(intento>14)
						             			{
						             				jugadores[pos].puntuacion=0;
						             				sprintf(estadisticas,"Usuario:%s. La puntuacion es la siguiente:%d, numero de intentos:%d\n", jugadores[pos].nombre, jugadores[pos].puntuacion, intento);//Escribimos en el buffer ya que hay que enviar una cadena
						             				send(i,estadisticas,strlen(estadisticas),0);
						             			}
						             		}
						             		else
						             			send(i,"+Ok. Le deseamos mejor suerte la proxima vez\n", strlen("+Ok. Le deseamos mejor suerte la proxima vez\n"),0);
						             }	
	   

		                        if(strcmp(buffer,"SALIR\n") == 0){
		                            
		                            salirCliente(i,&readfds,&numClientes,arrayClientes);
		                            
		                        }
		                        int fallo=comprobacionLineaOrdenes(buffer);
		                        if(fallo==1)
		                        	send(i,"-Err. Comando irreconocible\n", strlen("-Err. Comando irreconocible\n"),0);


		                        else{
		                            
		                            sprintf(identificador,"%d: %s",i,buffer);
		                            bzero(buffer,sizeof(buffer));
		                            strcpy(buffer,identificador);
		                            
		                            for(j=0; j<numClientes; j++)
		                                if(arrayClientes[j] != i)
		                                    send(arrayClientes[j],buffer,strlen(buffer),0);

		                            
		                        }
		                                                        
		                        
		                    }
		                    //Si el cliente introdujo ctrl+c
		                    if(recibidos== 0)
		                    {
		                        printf("El socket %d, ha introducido ctrl+c\n", i);
		                        //Eliminar ese socket
		                        salirCliente(i,&readfds,&numClientes,arrayClientes);
		                    }
		                }
		            }
		        }
		    }
		}

	close(sd);
	return 0;
	
}

void salirCliente(int socket, fd_set * readfds, int * numClientes, int arrayClientes[]){
  
    char buffer[250];
    int j;
    
    close(socket);
    FD_CLR(socket,readfds);
    
    //Re-estructurar el array de clientes
    for (j = 0; j < (*numClientes) - 1; j++)
        if (arrayClientes[j] == socket)
            break;
    for (; j < (*numClientes) - 1; j++)
        (arrayClientes[j] = arrayClientes[j+1]);
    
    (*numClientes)--;
    
    bzero(buffer,sizeof(buffer));
    sprintf(buffer,"Desconexión del cliente: %d\n",socket);
    
    for(j=0; j<(*numClientes); j++)
        if(arrayClientes[j] != socket)
            send(arrayClientes[j],buffer,strlen(buffer),0);


}


void manejador (int signum){
    printf("\nSe ha recibido la señal sigint\n");
    signal(SIGINT,manejador);
    
    //Implementar lo que se desee realizar cuando ocurra la excepción de ctrl+c en el servidor
}

//Comprueba usuarios
int comprobarJugadorUser(char cadNombre[50]){


	FILE *fich=NULL;
	char nombre[50];
	char pass[50];
	fich=fopen("users.txt", "r");
	int enc=0;

	if(fich){

	while(fscanf(fich, "%s %s", nombre, pass)==2){
		if(strcmp(cadNombre,nombre)==0){
			enc=1;
		}
	}

	fclose(fich);
	}
	else{
		printf("-Err.Error al abrir");
	}

return enc;

}
//Comprueba passwords
int validarJugadorPassword(char cadPass[50], char n[50]){


	FILE *fich=NULL;
	char nombre[50];
	char pass[50];
	fich=fopen("users.txt", "r");
	int enc=0;

	if(fich){

	while(fscanf(fich, "%s %s", nombre, pass)==2){
		if(strcmp(cadPass, pass)==0 && strcmp(nombre,n)==0){
			enc=1;
		}
	}

	fclose(fich);
	}
	else{
		printf("-Err.Error al abrir el fichero");
	}

return enc;

}
//Funcion que registra un jugador
int registrarJugador(char *user,char *pass){

	FILE *fich=NULL;

	int reg=0;

	fich=fopen("users.txt", "a");

	int encontrado= comprobarJugadorUser(user);
	if(encontrado==1)
		 {
		 	puts("-Err.Usuario ya existente");
		 	return 0;
		 }

	else{
			if(fich){

		fprintf(fich, "%s %s\n", user,pass);

		

		reg=1;
	}
	else{
		printf("-Err.Error al abrir el fichero");
	}


	}
fclose(fich);
	return reg;
}


//DEVUELVE UNA FRASE ALEATORIA DENTRO DEL FICHERO DE REFRANES
struct Partida devolverFrase()
{
    FILE *f;
    int nLines = 0;
    char line[1024];
    int randLine;
    int i;
    struct Partida p;

    srand(time(0));
    f = fopen("frases.txt", "r");

/* 1st pass - count the lines. */
    while(!feof(f))
    {
    	fgets(line, 1024, f);
    	nLines++;
    }

    randLine = rand() % nLines;

/* 2nd pass - find the line we want. */
    fseek(f, 0, SEEK_SET);
    for(i = 0; !feof(f) && i <= randLine; i++)
    	fgets(line, 1024, f);


    strcpy(p.frase_original,line);

    for(int i=0; i<strlen(p.frase_original)-1;i++)
			{
				if(p.frase_original[i]==' ')
					p.panel[i]=' ';
				else
				p.panel[i]='_';


			}

    return p;

}

//Destapa el panel sin descifrar
struct Partida destapar(struct Partida p, char letra)
{
//printf("%c\n", letra);
	p.encontrado=0;
	for (int i=0; i<strlen(p.frase_original); i++)
	{
		if(p.frase_original[i]==letra)
		{
			p.resuelto[i]=letra;
			p.encontrado=1;
		}


	}
	return p;
}

//Busca por el socket perteneciente a un usuario en el vector de jugadores
int buscaSocket(int i, int numClientes, struct Jugador *jugadores){

	int j;

	int pos;

	for(j=0;j<numClientes; j++){

		if(jugadores[j].socket==i){
			
			pos=j;	
		}		
	}
return pos;
}

//Funcion que comprueba que se escribe lo correcto por linea de ordenes
int comprobacionLineaOrdenes(char buffer[MSG_SIZE])
{
	int fallo=0;
	if(strncmp(buffer,"USUARIO",7)!=0 && strncmp(buffer,"PASSWORD",8)!=0 && strncmp(buffer,"REGISTRO",8)!=0 && strncmp(buffer,"SALIR",5)!=0 && strncmp(buffer,"PARTIDA-INDIVIDUAL",18)!=0 && strncmp(buffer,"PARTIDA-GRUPO",13)!=0 && strncmp(buffer,"CONSONANTE",10)!=0 && strncmp(buffer,"VOCAL",5)!=0 && strncmp(buffer,"RESOLVER",8)!=0 )
	{
		fallo=1;
	}
	return fallo;
}